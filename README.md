# README #

This repo is for updating the SAP Startup.Focus Workshop Documentation


### Organization ###

As you work on a file, take a copy of it from the Course Materials.SP11_AMI folder and place it in the Course Materials.SP12_AMI folder.  Change the revision inthe name and work on it there and be sure to commit all your changed files and git push them back into the repo.


### Handy Links ###

un: SYSTEM
pw: Welcome1

[ Links Tool ](http://links.sfphcp.com/)


### Contributors ###

* Andrew Lunde : andrew.lunde@sap.com
* Prashant Jayaraman : prashant.jayaraman@sap.com
* Nitish Kumar Agrawal : nitish.agrawal@sap.com
